#!/bin/bash
#
# This script will get all mongo containers ID from all RPIs
# 
# Usage: ./Init_ReplicaSet <numéro raspberry primary> > numéro dernière raspberry dans réplica>

sudo apt-get install sshpass



for i in $(seq $1 $2)
do
    echo Get id from pi@10.45.0.$i ...
    sshpass -p 1234 ssh -o "StrictHostKeyChecking no" pi@10.45.0.$i "sudo docker ps -q -f name=mongo" >> file.txt
    sshpass -p 1234 ssh -o "StrictHostKeyChecking no" pi@10.45.0.$i "sudo docker ps -q -f name=mongo"
    if [ $i == $1 ]; then
        sudo docker ps -q -f name=mongo >> test.txt
        sudo docker ps -q -f name=mongo
        read primaryContainer < test.txt
    fi
done


echo

echo Creating js script file ...

echo 'rs.initiate();' >> init_mongo.js

var=0

while IFS= read -r line || [[ -n "$line" ]]; do
    
    echo "rs.add(\"$line:27017\");" >> init_mongo.js
    var=$((var+1))
     
done < file.txt

echo 'cfg = rs.conf();' >> init_mongo.js
echo 'cfg.members[0].priority = 1;' >> init_mongo.js


for i in $(seq 1 $var)
do
    echo "cfg.members[$i].priority = 1 - 0.05*$i;" >> init_mongo.js
done

echo 'rs.reconfig(cfg);' >> init_mongo.js

echo Done

echo

echo Run script inside mongo container ...

docker cp init_mongo.js $primaryContainer:/init_mongo.js

docker exec $primaryContainer bash -c 'mongo < init_mongo.js'

rm test.txt
rm file.txt
rm init_mongo.js


echo Done