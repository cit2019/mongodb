# MongoDB

Docker image for deploying MongoDB

## Deployment

```bash
docker network create --driver=overlay --attachable mongo-external
docker stack deploy --resolve-image=never --compose-file stack.yml mongo
```
