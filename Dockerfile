FROM resin/rpi-raspbian

# install required packages
RUN apt-get update && apt-get upgrade
RUN apt-get install mongodb
RUN apt-get install iputils-ping

# Expose ports.
#   - 27017: process
#   - 28017: http
EXPOSE 27017
EXPOSE 28017

RUN mkdir -p /data/db
#RUN mongo
#RUN rs.initiate( {
#   _id : "rs0",
#   members: [ { _id : 0, host : "mongo0:27017" } ]
#})

#RUN use admin
#RUN db.createUser({user:"foouser",pwd:"foopwd",roles:[{role:"root",db:"admin"}]})

CMD mongod --replSet "rs0" --smallfiles --journal



